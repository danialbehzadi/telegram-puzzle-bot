#! /usr/bin/env python2
# Danial Behzadi - 2016
# Published under AGPLv3+
#
import telebot
import SNserver

TOKEN = "TOKEN"
bot = telebot.TeleBot(TOKEN)

@bot.message_handler(commands=['start'])
def send_welcome(message):
    chatid = message.chat.id
    markup = telebot.types.ReplyKeyboardMarkup()
    markup.row('/join', '/cancel')
    bot.send_message(chatid, "Hi, join the game or cancel it.", reply_markup=markup)
#    if (message.chat.type == "private"):
#        start_game(message)
#    else:
#        markup = telebot.types.ReplyKeyboardMarkup()
#        markup.row('/join', '/cancel')
#        bot.send_message(chatid, "Hi, join the game or cancel it.", reply_markup=markup)

@bot.message_handler(commands=['join'])
def join_game(message):
    chatid = message.chat.id
    sender = message.from_user
    markup = telebot.types.ReplyKeyboardMarkup(selective=True)
    markup.row('/start_game', '/cancel')
    bot.send_message(chatid, "%s joined the game (:" % sender.first_name, reply_markup=markup, reply_to_message_id=message.message_id)
    SNserver.addplayer(chatid, sender.id, sender.first_name)

@bot.message_handler(commands=['start_game'])
def start_game(message):
    chatid = message.chat.id
    sender = message.from_user
    markup = telebot.types.ReplyKeyboardHide(selective=False)
    bot.send_message(chatid, "The game has been started!", reply_markup=markup)
    SNserver.changeturn(chatid, sender.id)
    markup = telebot.types.ForceReply(selective=True)
    bot.send_message(chatid, "Please reply your photo.", reply_markup=markup, reply_to_message_id=message.message_id)

@bot.message_handler(commands=['cancel'])
def cancel_game(message):
    chatid = message.chat.id
    sender = message.from_user
    markup = telebot.types.ReplyKeyboardHide(selective=False)
    bot.send_message(chatid, "The game has been canceled ):", reply_markup=markup)
    SNserver.remove(chatid)

@bot.message_handler(func=lambda message: True, content_types=['photo'])
def is_photo(message):
    chatid = message.chat.id
    sender = message.from_user
    if (SNserver.joined(chatid, sender.id)):
        if (SNserver.on_turn(chatid, sender.id)):
            chatid = message.chat.id
            try:
                fileid = message.photo[2].file_id
            except IndexError:
                fileid = message.photo[1].file_id
            path=bot.get_file(fileid).file_path
            address="https://api.telegram.org/file/bot"+TOKEN+"/"+path
            image_name = str(chatid)+".jpeg"
            SNserver.puzzlise(address, chatid)
            SNserver.draw(chatid)
            bot.send_chat_action(chatid, "upload_photo")
            photo = open(image_name, 'rb')
            bot.send_photo(chatid, photo)
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            markup.row('/b1', '/b2', '/b3')
            markup.row('/b4', '/b5', '/b6')
            bot.send_message(chatid, "Which part this should be replaced with?\nIt's %s's turn" % sender.first_name, reply_markup=markup)
        else:
            name = SNserver.whoesturn(chatid)
            bot.send_message(chatid, "You can not upload photo! Only %s can do this" % name , reply_to_message_id=message.message_id)
    else:
        bot.send_message(chatid, "You are not in the game. You can't upload photo!" , reply_to_message_id=message.message_id)

@bot.message_handler(commands=['b1', 'b2', 'b3', 'b4', 'b5', 'b6'])
def change_by_0(message):
    chatid = message.chat.id
    sender = message.from_user
    data = SNserver.readjson(chatid)
    players = data['players']
    names = data['names']
    order = data['order']
    part = (data['part']+1)%len(order)
    turn = data['turn']
    reigon = data['reigon']
    address = data['address']
    if (SNserver.joined(chatid, sender.id)):
        if (SNserver.on_turn(chatid, sender.id)):
            image_name = str(chatid)+".jpeg"
            num=int(message.text[-1])-1
            temp = order[part]
            order[part] = order[num]
            order[num] = temp
            if (order == [0, 1, 2, 3, 4, 5]):
                SNserver.finish(chatid)
                markup = telebot.types.ReplyKeyboardHide(selective=False)
                bot.send_message(chatid, "Congratulations!", reply_markup=markup)
                bot.send_chat_action(chatid, "upload_photo")
                photo = open(image_name, 'rb')
                bot.send_photo(chatid, photo)
                SNserver.remove(chatid)
            else:
                turn = (turn+1)%len(players)
                SNserver.writejson(chatid, players, names, order, reigon, address, part, turn)
                SNserver.draw(chatid)
                bot.send_chat_action(chatid, "upload_photo")
                photo = open(image_name, 'rb')
                bot.send_photo(chatid, photo)
                name = SNserver.whoesturn(chatid)
                markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
                markup.row('/b1', '/b2', '/b3')
                markup.row('/b4', '/b5', '/b6')
                bot.send_message(chatid, "It's %s's turn" % name, reply_markup=markup)
        else:
            bot.send_message(chatid, "%s, it's not your turn. Please be patient!" % sender.first_name, reply_to_message_id=message.message_id)
    else:
        bot.send_message(chatid, "%s, you are not in the game. wait for the next round" % sender.first_name, reply_to_message_id=message.message_id)

bot.polling()
