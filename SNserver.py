#! /usr/bin/env python3
# Danial Behzadi - 2016
# Published under AGPLv3+
#
import io
import os
import urllib
import json
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from random import sample
from StringIO import StringIO

#configs
xpieces , ypieces = 3, 2
choices = 4

def addplayer(chatid, player, name):
    try:
        data = readjson(chatid)
        players = data['players']
        players.append(player)
        names = data['names']
        names.append(name)
        writejson(chatid, players, names)
    except IOError:
        writejson(chatid, [player], [name])

def changeturn(chatid, player):
    data = readjson(chatid)
    players = data['players']
    turn = players.index(player)
    writejson(chatid, players, data['names'], data['order'], data['reigon'], data['address'], data['part'], turn)

def joined(chatid, player):
    data = readjson(chatid)
    players = data['players']
    if (player in players):
        return True
    else:
        return False

def on_turn(chatid, player):
    data = readjson(chatid)
    players = data['players']
    turn = data['turn']
    if (player == players[turn]):
        return True
    else:
        return False

def whoesturn(chatid):
    data = readjson(chatid)
    turn = data['turn']
    name = data['names']
    return name[turn]

def puzzlise(address, chatid):
    try:
        df = urllib.urlopen(address)
        image_file = io.BytesIO(df.read())
        image = Image.open(image_file)
        image.save(str(chatid)+'.jpeg', "JPEG")
        xsize, ysize = image.size
        boxw, boxh = int(xsize/xpieces), int(ysize/ypieces)
        hsize, vsize = boxw*xpieces, boxh*ypieces
        reigon=[]
        order = sample(range(0, xpieces*ypieces), xpieces*ypieces)
        for ypiece in range(ypieces):
            for xpiece in range(xpieces):
                reigon.append((xpiece*boxw, ypiece*boxh, (xpiece+1)*boxw, (ypiece+1)*boxh))
        data = readjson(chatid)
        part = -1
        writejson(chatid, data['players'], data['names'], order, reigon, address, part, data['turn'])
        box = boxify(chatid)
    except IOError:
        print("There's something wrong with the image")

#def draw(order, box, reigon, name, part):
def draw(chatid):
    data = readjson(chatid)
    part = data['part']
    order = data['order']
    reigon = data['reigon']
    address = data['address']
    box = boxify(chatid)
    image_name = str(chatid)+".jpeg"
    image = Image.open(image_name)
    for piece in range(len(order)):
        image.paste(box[order[piece]], reigon[piece])
    place = Image.new("RGBA", box[0].size, color=(255, 0, 0, 64))
    image.paste(place, reigon[(part+1)%len(order)], place)
    number = ImageDraw.Draw(image)
    font = ImageFont.truetype("times.ttf", 160)
    piece = 1
    for location in reigon:
        a, b, c, d = location
        number.text((a, b), str(piece), (255, 255, 255), font)
        piece += 1
    image.save(image_name, "JPEG")

def boxify(chatid):
    data = readjson(chatid)
    reigon = data['reigon']
    address = data['address']
    box=[]
    df = urllib.urlopen(address)
    image_file = io.BytesIO(df.read())
    image = Image.open(image_file)
    for piece in reigon:
        part = image.crop(piece)
        box.append(part)
    return box

def writejson(chatid, players=[], names=[], order=[], reigon=[], address='', part=-1, turn=0):
    data = {'players':players, 'names':names, 'turn':turn, 'part':part, 'order':order, 'reigon':reigon, 'address':address}
    with open(str(chatid)+'.json', 'w') as df:
        json.dump(data, df)

def readjson(chatid):
    with open(str(chatid)+'.json') as df:
        data = json.load(df)
        return data

def finish(chatid):
    data = readjson(chatid)
    reigon = data['reigon']
    box = boxify(chatid)
    image_name = str(chatid)+'.jpeg'
    image = Image.open(image_name)
    for piece in range(len(box)):
        image.paste(box[piece], reigon[piece])
    image.save(image_name, "JPEG")

def remove(chatid):
    try:
        os.remove(str(chatid)+'.jpeg')
        os.remove(str(chatid)+'.json')
    except OSError:
        pass
